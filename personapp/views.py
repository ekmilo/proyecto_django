from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.
def hola(request,parametro1,parametro2,clave,nombre):
    return HttpResponse("p1 %s p2 2 %s clave %s nombre %s" % (parametro1,parametro2,clave,nombre))

def test(request):
    return HttpResponse("prueba persona " )

def indice(request):
    return render(request,"personapp/listar.html")

def error(request):
    return render(request,"admin/error.html")
