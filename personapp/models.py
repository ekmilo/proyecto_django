from django.db import models

# Create your models here.
class Persona(models.Model):
    nombre=models.CharField(max_length=100)
    edad=models.IntegerField()
    cedula=models.IntegerField()

    def __str__(self):
        return("Persona %s %r %s" % (self.nombre, self.edad, self.cedula))
