from django.http import HttpResponse, HttpResponseNotFound, Http404
from django.views.decorators.http import require_http_methods,require_POST,require_GET
from .models import Persona
from django.shortcuts import render,render_to_response,redirect,get_object_or_404,HttpResponseRedirect
from django.core.urlresolvers import reverse

@require_GET
def listar(req,idpersona):
    #persona=get_object_or_404(Persona, pk=int(idpersona))
    try:
        persona = Persona.objects.get(pk = int(idpersona))
    except Exception as e:
        #return HttpResponseNotFound("no se encontró el objeto")
        raise Http404("no se encontró")
    #return HttpResponse("persona %s" % (persona.nombre))
    return render(req,"personapp/listar.html",{'persona':persona})
    #return render_to_response("personapp/listar.html",{'persona':persona})
    #return redirect("crear")
    #return HttpResponseRedirect(reverse("crear"))

def crear(req,nombre,edad):
    persona=Persona(nombre=nombre,edad=edad,cedula=0)
    persona.save()
    return HttpResponse("creado exitosamente persona: %s " % (persona))

def actualizar(req,idpersona,nombre):
    persona=Persona.objects.get(pk = int(idpersona))
    persona.nombre=nombre
    persona.save()
    return HttpResponse("fue actualizado persona %s" % (persona))

def borrar(req,idpersona):
    persona=Persona.objects.get(pk=int(idpersona))
    persona.delete()
    return HttpResponse("persona borrada")

def todo(req):
    lista=Persona.objects.all()
    resultado=', '.join([persona.nombre for persona in lista])
    #return HttpResponse(resultado)
    return render_to_response("personapp/todo.html",{'lista':lista})
