from django.contrib import admin
from .models import Persona

# Register your models here.
class PersonaAdmin(admin.ModelAdmin):
    fields=['cedula','nombre','edad']

class PersonaAdmin1(admin.ModelAdmin):
    fieldsets=[
    ('datos primarios',{'fields':['cedula'] }),
    ('datos secundarios', {'fields':['nombre','edad']})
    ]

admin.site.register(Persona,PersonaAdmin1)
