from django.conf.urls import url
from . import views
from . import crud

urlpatterns=[
    url(r'^hola/([0-9]{1,10})a([a-z]{1,10})/$',
    views.hola,{'clave':'valor','nombre':'ana'}),
    url(r'^prueba/(?P<nombrepersona>[a-z]{1,5})/$',views.test,name="test"),
    url(r'^index/$',views.indice,name="indice"),
    url(r'^listar/(?P<idpersona>[0-9]{1,10})/$',crud.listar),
    url(r'^crear/(?P<nombre>[a-zA-Z]{1,10})/(?P<edad>[0-9]{1,10})$',crud.crear,name="crear"),
    url(r'^actualizar/(?P<idpersona>[0-9]{1,10})/(?P<nombre>[a-zA-Z]{1,10})$',crud.actualizar),
    url(r'^borrar/(?P<idpersona>[0-9]{1,10})$',crud.borrar),
    url(r'^todo$',crud.todo),
]
